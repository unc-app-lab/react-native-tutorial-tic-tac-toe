# React Native Tutorial: Tic-Tac-Toe

This is a simple React Native app to play a game of tic-tac-toe. You can either clone this repo and use the app yourself (see next section), or try following along with each step of the development for a friendlier tutorial.

### Usage

- Using your phone's app store (iOS or Android), download the "Expo Client" app on your phone.
- Clone this repo and `cd` to the directory.
- Run `npm install`
- Run `npm start`, which will start a development server and open a dashboard in a web page.
- Using your phone, scan the QR code shown in the dashboard (on iOS, you can use the built-in Camera app for this) to launch the app on your phone.
- If you change the source code (e.g. change the backgroundColor from 'beige' to 'green') and save the file, the app on your phone should reload automatically.

### Tutorial

This repo corresponds with a talk given by Dr. Jeff Terrell on Tuesday, October 2nd, 2018, as part of the preliminaries for the 2018 [HackNC](https://hacknc.com/) hackathon. You can view [a recording of the talk on YouTube](https://www.youtube.com/watch?v=o4364gLHq6M).

You can follow the steps from Dr. Terrell's talk by checking out the code at each tag from `step01` to `step10`. Here is a list of the development steps corresponding to those tags. (The tag itself points to the completion of the step.) Included are specific concepts covered by each step. (For more info, check out [the detailed talk notes](https://gitlab.com/unc-app-lab/react-native-tutorial-tic-tac-toe/blob/master/talk-notes.org).)

- [`step01`](https://gitlab.com/unc-app-lab/react-native-tutorial-tic-tac-toe/commit/22486950b53f6e02cfcbd84820751f13acf44ce2): initialize the project (by running `expo init hacknc` and choosing the `blank` template)
  - get started on a React Native mobile app quickly with the `expo` tool
  - use both your computer and your phone to develop a mobile app, linked by the `expo` tool
- [`step02`](https://gitlab.com/unc-app-lab/react-native-tutorial-tic-tac-toe/commit/b456423692ee24d74efe2584ab71f6c9998836e1): create a wireframe for the board: covers flexbox layout and CSS-esque styling.
  - markup looks a lot like HTML, but with different tag names
  - layout uses flexbox positioning (see [Flexbox Froggy](http://flexboxfroggy.com) to learn it)
  - many familiar CSS rules work to style elements
  - the `<SafeAreaView>` component in React Native to avoid the phone's controls
- [`step03`](https://gitlab.com/unc-app-lab/react-native-tutorial-tic-tac-toe/commit/133cc63f71a64ab59c8716c6c7e2247bd9f56881): add state and render squares from the state: illustrates the basic state -> markup flow of React and React Native.
  - class-based components (but not function-based components) can have state
  - **your rendered output should depend on your state in some way**
  - components can pass data as 'props' or properties
- [`step04`](https://gitlab.com/unc-app-lab/react-native-tutorial-tic-tac-toe/commit/27a6962593273f394e0e0299040bc4f3fd249cc6): handle touches on the squares: illustrates using callbacks to change the state.
  - use methods to modify state in a class-based component
  - pass methods as props to other components so they can call them when needed
  - be sure to set your `this` reference correctly when passing a method as a prop
  - the `TouchableHighlight` component in React Native
- [`step05`](https://gitlab.com/unc-app-lab/react-native-tutorial-tic-tac-toe/commit/2cbfccb684c75ef1ffc9e8ed27158fb9212b7996): track whose turn it is: add another dimension to the state.
- [`step06`](https://gitlab.com/unc-app-lab/react-native-tutorial-tic-tac-toe/commit/39fbdccaca81902a5cd054351569c48bc8536cb1): display turn indicator to user.
- [`step07`](https://gitlab.com/unc-app-lab/react-native-tutorial-tic-tac-toe/commit/629449301fa79ebcff037b7860cad84ff81cdf9e): display winning message.
  - not all logic has to live in components; sometimes a function is simpler
- [`step08`](https://gitlab.com/unc-app-lab/react-native-tutorial-tic-tac-toe/commit/2d76fc60901e3aa4982527ce94f1d45471b01717): guard against illegal moves.
  - state-changing methods can skip the state-changing statement if needed
- [`step09`](https://gitlab.com/unc-app-lab/react-native-tutorial-tic-tac-toe/commit/424aca144d4f51cfa55e197438040f2a9feba52f): display tie game message.
  - usually a view change goes along with a state/logic change
- [`step10`](https://gitlab.com/unc-app-lab/react-native-tutorial-tic-tac-toe/commit/cdb81729a79084beaff9e9e23327201e30ee2177): add 'new game' button.
  - the `<Button>` component in React Native
